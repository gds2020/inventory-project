﻿using System;





class Weapon : Equipment
{
    public float damage;
    public bool isMelee;


    public virtual void Attack()
    {
        Console.WriteLine("undefined weapon attack");
    }


    public override string ToString()
    {
        return string.Format("{0}damage={1}\nisMelee={2}\n",
            base.ToString(), damage, isMelee);
    }
}
