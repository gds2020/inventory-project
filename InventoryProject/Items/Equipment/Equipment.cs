﻿



class Equipment : Item
{
    public float durability;


    public override string ToString()
    {
        return string.Format(
            "{1}durability={0}\n",
            durability,
            base.ToString());
    }

}
