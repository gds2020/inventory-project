﻿using System;



class TwoHandedSword : Sword
{
    public TwoHandedSword()
    {
        isDoubleHanded = true;
    }

    public override void Attack()
    {
        Console.WriteLine("chopping with great sword");
    }
}
