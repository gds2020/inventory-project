﻿using System;


class Bow : Weapon
{
    public float shotDistance;



    public override void Attack()
    {
        Console.WriteLine("arrows flyes into the sky");
    }


    public override string ToString()
    {
        return string.Format("{0}shotDistance={1}",
            base.ToString(), shotDistance);
    }


    public Bow() : this(10.1f) { }


    public Bow(float dmg) : this(dmg, 101f) { }


    public Bow(float dmg, float distance) : this(dmg, distance, 2.55f) { }


    public Bow(float dmg, float distance, float wgth)
    {
        damage = dmg;
        shotDistance = distance;
        weight = wgth;
        name = "Generic Bow";
        volume = 6;
        durability = 100f;
        isMelee = false;
    }
}
