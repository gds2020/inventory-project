﻿using System;



class Item
{
    public string name;
    public int volume;
    public float weight;

    public override string ToString()
    {
        return string.Format(
            "name={0}\nvolume={1}\nweigth={2}\n",
            name, volume, weight);
    }
}
