﻿using System;


class Food : Consumable
{
    public Food()
    {
        this.name = "Bread";
        this.volume = 2;
        this.weight = 0.7f;
        this.usages = 3;
        this.amount = 200f;
    }


    public override bool Boost(Player player)
    {
        bool ok = base.Boost(player);

        if (ok)
        {
            player.hunger -= amount;
            if (player.hunger < 0f)
                player.hunger = 0f;
        }
        return ok;
    }
}
