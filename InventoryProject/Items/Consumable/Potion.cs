﻿



class Potion : Consumable
{
    public Potion()
    {
        this.name = "Undefined Potion";
        this.volume = 2;
        this.weight = 0.1f;
        this.usages = 1;
        this.amount = 55f;
    }


    public override bool Boost(Player player)
    {
        if (base.Boost(player))
        {
            player.strength += amount;
            player.dexterity += amount;
            return true;
        }
        return false;
    }
}
