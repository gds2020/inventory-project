﻿using System;



class Medkit : Consumable
{
    public Medkit()
    {
        this.name = "Medical Kit";
        this.volume = 1;
        this.weight = 0.2f;
        this.usages = 5;
        this.amount = 20f;
    }

    

    public override bool Boost(Player player)
    {
        if (base.Boost(player))
        {
            player.health = Math.Min(player.health + amount, 100f);
            return true;
        }
        return false;
    }
}
