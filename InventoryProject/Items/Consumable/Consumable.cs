﻿



class Consumable : Item
{
    public int usages;
    public float amount;


    public virtual bool Boost(Player player)
    {
        if (usages > 0)
            usages -= 1;
        return usages > 0;
    }


    public override string ToString()
    {
        return string.Format("{0}usages={1}\namount={2}\n",
            base.ToString(), usages, amount);
    }
}
