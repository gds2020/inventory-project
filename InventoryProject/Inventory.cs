﻿using System;


class Inventory
{
    private Item[] items;
    private int count;


    public Inventory()
    {
        items = new Item[8];
        count = 0;
    }



    public void AddUnit(Item unit)
    {
        Program.Assert(unit != null, "unit is null");
        Program.Assert(items != null, "no array to add to");

        int emptyElements = items.Length - count;
        if (emptyElements <= 0)
            EnlargeCapacity();

        items[count++] = unit;
    }


    public void RemoveUnit(Item unit)
    {
        int idx = -1;
        for (int i = 0; i < count; i++)
            if (unit == items[i])
            {
                idx = i;
                break;
            }

        for (int i = idx; i < count - 1; i++)
            items[i] = items[i + 1];

        count -= 1;
        items[count] = null;
    }


    public float GetTotalWeigth()
    {
        float weigth = 0f;

        for (int i = 0; i < count; i++)
            weigth += items[i].weight;

        return weigth;
    }


    public string[] GetAllNames()
    {
        string[] allNames = new string[count];

        for (int i = 0; i < count; i++)
            allNames[i] = items[i].name;

        return allNames;
    }


    public Consumable[] GetConsumables()
    {
        int num = 0;
        for (int i = 0; i < count; i++)
        {
            if (items[i] is Consumable)
                num++;
        }

        Consumable[] arr = new Consumable[num];
        int k = 0;
        for (int i = 0; i < count; i++)
        {
            Consumable c = items[i] as Consumable;
            if (c != null)
                arr[k++] = c;
        }

        return arr;
    }


    private void EnlargeCapacity()
    {
        Item[] newItems = new Item[items.Length * 2];

        for (int i = 0; i < items.Length; i++)
            newItems[i] = items[i];

        items = newItems;
    }
}



// .
// *
// .
// .
// *
// .
// .

// * *