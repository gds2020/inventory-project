﻿



class Player : Character
{
    public Inventory inventory;
    public float hunger;
    public float strength;
    public float dexterity;

    public Coord pos;


    public Player(string name) : base(name, 100f)
    {
        inventory = new Inventory();
        hunger = 0f;
        strength = 1f;
        dexterity = 1f;
    }

}
