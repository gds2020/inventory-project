﻿using System;


class Character
{
    public string name;
    public float health;


    public Character(string name, float health)
    {
        this.name = name;
        this.health = health;
    }
}
