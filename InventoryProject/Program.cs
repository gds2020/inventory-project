﻿using System;
using System.IO;



class Program
{
    public const int X = 20, Y = 10;

    static void Main(string[] args)
    {
        Console.SetWindowSize(X, Y);
        Console.CursorVisible = false;


        Player player = new Player("Derek");

        player.pos.x = 3;
        player.pos.y = 4;

        Console.SetCursorPosition(player.pos.x, player.pos.y);
        Console.Write('@');


        while (true)
        {
            ConsoleKey key = Console.ReadKey().Key;

            Console.SetCursorPosition(player.pos.x, player.pos.y);
            Console.Write(' ');

            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    player.pos.x--;
                    if (player.pos.x < 0)
                        player.pos.x = X - 1;
                    break;

                case ConsoleKey.RightArrow:
                    player.pos.x++;
                    if (player.pos.x >= X)
                        player.pos.x = 0;
                    break;

                case ConsoleKey.DownArrow:
                    player.pos.y++;
                    if (player.pos.y >= Y)
                        player.pos.y = 0;
                    break;

                case ConsoleKey.UpArrow:
                    player.pos.y--;
                    if (player.pos.y < 0)
                        player.pos.y = Y - 1;
                    break;

                case ConsoleKey.Escape:
                    System.Environment.Exit(0);
                    break;
            }
        }
    }


    public static void Assert(bool condition, string message)
    {
        if (condition == false)
        {
            Console.WriteLine(message);
            Environment.Exit(1);
        }
    }
}



/*
 * Inventory - can store items
 * Items
 *      - firearms
 *      - melee weapons
 *      - magic weapons
 *      - usabele items
 *          - healing potions
 *          - magic potions
 *          - stat potions
 *          - food
 *          - drinks
*/
